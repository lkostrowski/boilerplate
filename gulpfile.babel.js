// package imports
import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import size from 'gulp-size';
import sourcemaps from 'gulp-sourcemaps';
import livereload from 'gulp-livereload';
import notify from 'gulp-notify';
import concat from 'gulp-concat';
import jshint from 'gulp-jshint';
import rename from 'gulp-rename';
import environments from 'gulp-environments';
import htmlhint from 'gulp-htmlhint';
import jscs from 'gulp-jscs';
import jscsStylish from 'gulp-jscs-stylish';
import bless from 'gulp-bless'

// some basic configuration
const config = {
  theme: 'theme/',
  js : 'theme/js',
};


// compile SASS files
gulp.task('styles', () => {

  // define set of browsers for autoprefixer
  const BROWSERS = [
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10',
  ];

  // compile sass
  return gulp.src(config.theme + 'scss/styles.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', notify.onError('<%= error.message %>')))
      .pipe(autoprefixer(BROWSERS))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(config.theme + 'css/'))
});






gulp.task('jscs', function() {
    
    const what=gulp.src([config.js + '/shared/*.js', config.js + '/modules/*.js', config.theme + '/**/*.js'])

    return what
        .pipe(jscs())
        .pipe(jscs.reporter())
        //.pipe(jshint.reporter('fail'));  
    
});






// Lint Task
gulp.task('lint', ['jslint', 'jscs']);



// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src([  config.js + 'modules/global.js', config.js + '/modules/*.js', config.js + '/pages/*.js'])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(config.js))
        //.pipe(rename('all.min.js'))
        //.pipe(uglify())
        //.pipe(gulp.dest('js'))
        ;
});



// vendors js are kept separately
gulp.task('vendors', function() {
    return gulp.src([ config.js + 'vendors/jquery.min.js', config.js + '/vendors/*js'])
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(config.js))
        //.pipe(rename('all.min.js'))
        //.pipe(uglify())
        //.pipe(gulp.dest('js'))
        ;
});



// watchers
gulp.task('watch', () => {

  // Create LiveReload server
  livereload.listen();

  // watch for SASS files
  gulp.watch([config.theme + 'scss/**/*.scss'], ['styles']);

  // watch for JS files
gulp.watch([config.js + '/**/*.js', config.theme + '/**/*.js'] , ['scripts']);
//gulp.watch([config.js + '/**/*.js', config.theme + '/**/*.js'], ['lint']);

});



// Builds and checks files
gulp.task('build', ['styles', 'vendors', 'scripts', 'lint']);



// Performs build and watches for changes
gulp.task('default', ['build', 'watch']);
